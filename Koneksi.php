<?php

error_reporting(0);
class Koneksi {

    public function cek_koneksi($hostname,$username,$pass="",$dbname)
    {
        if (empty($hostname) OR empty($dbname) OR empty($username)) {
            return $this->json(true,"required parameter is empty");
        } else {
            $mysqli = new mysqli($hostname,$username,$pass,$dbname);
            if ($mysqli->connect_errno) {
                return $this->json(true,"unknown database");
            } else {
                return $this->json(false,"koneksi berhasil");
            }
            
        }
    }

    public function json($error,$data)
	{
        $json = array();
        $json['error'] = $error;
        $json['msg'] = $data;

        header('Content-Type: application/json');
        echo json_encode($json);
    }

    
}

$hostname = $_POST['hostname'];
$username = $_POST['username'];
$pass = $_POST['pass'];
$dbname = $_POST['name'];
$koneksi = new Koneksi();
$koneksi->cek_koneksi($hostname,$username,$pass,$dbname);

?>