<?php

require_once('vendor/autoload.php');
use Faker\Factory;
use Medoo\Medoo;

class Generate {

		public $host;
		public $username;
		public $pass;
		public $db;

		public $array = []; //menampung format data
		public $data = []; //menampung data jadi
		public $row;

		public function db_set($host,$username,$pass,$db)
		{
			$this->host = $host;
			$this->username = $username;
			$this->pass = $pass;
			$this->db = $db;
		}

		public function data_former($post)
		{
			$raw_data = array_slice($post,5,intval($post['len'])*2);

			$keys = array();
			$val = array();

			$data = [];

			$a = 1;
			foreach ($raw_data as $key => $value) {
				if ($a % 2 == 0) {
					array_push($val, $value);
				} else {
					array_push($keys, $value);
				}
				$a++;
			}

			for ($i=0; $i < count($keys) ; $i++) { 
				$data[$keys[$i]] = $val[$i];
			}

			return $this->array = $data;
		}

		public function data_builder($amount)
		{
			for ($i=0; $i < $amount ; $i++) { 
				$temp = array();
				foreach ($this->array as $key => $value) {
					$val = $this->sel_faker($value);
					$temp[$key] = $val;
				}
				array_push($this->data, $temp);
			}
		}

		public function sel_faker($type)
		{
			$faker = Factory::create('id_ID');

			switch ($type) {
				case 'username':
					return $faker->username;
					break;
				case 'password':
					return $faker->password;
					break;
				case 'address':
					return $faker->address;
					break;
				case 'firstname':
					return $faker->firstname;
					break;
				case 'lastname':
					return $faker->lastname;
					break;
				case 'email':
					return $faker->safeEmail;
					break;
				case 'country':
					return $faker->country;
					break;
				case 'city':
					return $faker->city;
					break;
				case 'phonenumber':
					return $faker->e164PhoneNumber;
					break;
				case 'ipv4':
					return $faker->ipv4;
					break;
				case 'latitude':
					return $faker->latitude;
					break;
				case 'longitude':
					return $faker->longitude;
					break;
				case 'date':
					return $faker->date;
					break;
				case 'time':
					return $faker->time;
					break;
				case 'datetime':
					return $faker->dateTime;
					break;
				default:
					# code...
					break;
			}
		}

		public function generate ($table="")
		{
			$db = new Medoo([
				'database_type' => 'mysql',
				'database_name' => $this->db,
				'server' => $this->host,
				'username' => $this->username,
				'password' => $this->pass
			]);

			$query = $db->insert($table,$this->data);
			return $this->row = $query->rowCount();
		}

}


$gen = new Generate();

if (isset($_POST['dbhostname']) OR isset($_POST['dbusername']) OR isset($_POST['dbpass']) OR isset($_POST['dbname']) OR isset($_POST['data-amount']) OR isset($_POST['table-name'])) {
	$gen->db_set($_POST['dbhostname'],$_POST['dbusername'],$_POST['dbpass'],$_POST['dbname']);
	$gen->data_former($_POST);
	$gen->data_builder($_POST['data-amount']);
	$gen->generate($_POST['table-name']);
	
	$json = array();
	
	if ($gen->row > 0) {
		$json['error'] = false;
	} else {
		$json['error'] = true;
	}

	$json['row'] = $gen->row;
	header("Content-Type: application/json");
	echo json_encode($json);

} else {
	// print_r($_POST);
}




?>