<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Faker</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="assets/fa/css/all.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <style>
      body {
        padding-top: 54px;
      }
      @media (min-width: 992px) {
        body {
          padding-top: 56px;
        }
      }

    </style>

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <h3 class="text-white">Faker</h3>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="mt-5">Generate Dummy Data for your Database</h2>
          <p class="lead">benben4567@gmail.com</p>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg-10">
        <form id="main-form">
          <!-- DB Config -->
          <div class="col-lg-12 mt-3">
            <div class="card">
              <div class="card-header bg-dark text-white">Database Configuration</div>
              <div class="card-body">
                  <div class="form-group">
                    <label for="db-hostname">Host Name/IP Address :</label>
                    <input type="text" class="form-control" name="dbhostname" value="localhost" required>
                  </div>
                  <div class="form-group">
                    <label for="db-username">Port :</label>
                    <input type="text" class="form-control" name="dbusername" value="root" required>
                  </div>
                  <div class="form-group">
                    <label for="db-pass">Password :</label>
                    <input type="password" class="form-control" name="dbpass">
                  </div>
                  <div class="form-group">
                    <label for="db-name">DB Name :</label>
                    <input type="text" class="form-control" name="dbname" required>
                  </div>
                  <button class="btn btn-info mb-3" id="cek-koneksi"><i class="fas fa-link"></i> Cek Koneksi</button>
              </div> 
            </div>
          </div>
          <!-- TABLE Config -->
          <div class="col-lg-12 mt-3" id="table-config">
            <div class="card">
              <div class="card-header bg-dark text-white">Table Configuration</div>
              <div class="card-body">
                  <div class="form-group">
                    <label for="table-name">Table Name :</label>
                    <input type="text" class="form-control" name="table-name">
                  </div>

                  <div class="row data-row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="column-name">Kolom 1 :</label>
                        <input type="text" class="form-control col-name" name="col-1" placeholder="Nama Kolom">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="data-type">Tipe Data : </label>
                        <select class="form-control data-type" name="data-1">
                          <option default>-- pilih --</option>
                          <option value="lastname">lastname</option>
                          <option value="firstname">firstname</option>
                          <option value="username">username</option>
                          <option value="email">email</option>
                          <option value="password">password</option>
                          <option value="address">address</option>
                          <option value="country">country</option>
                          <option value="city">city</option>
                          <option value="phonenumber">phone number</option>
                          <option value="date">date</option>
                          <option value="time">time</option>
                          <option value="datetime">date time</option>
                          <option value="latitude">latitude</option>
                          <option value="longitude">longitude</option>
                          <option value="ipv4">ipv4</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  
                  <button id="btn-tambah" class="btn btn-primary mt-3">	<i class="fas fa-plus"></i>	Tambah Kolom</button>

                  <div class="form-group mt-3" id="amount">
                    <label for="data-amount">Jumlah : </label>
                    <input type="number" class="form-control" name="data-amount">
                  </div>
                  <input type="text" name="len" style="display: none;">
              </div> 
            </div>
          </div>
          <!-- Button Submit -->
          <div class="col-lg-12 mt-3 mb-3">
              <button id="btn-submit" class="btn btn-block btn-success">SUBMIT</button>
            </div>
        </form>
        </div>
      </div>
    </div>



    <!-- Bootstrap core JavaScript -->
    <script src="assets/jquery/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Sweetalert2 -->
    <script src="assets/swal2/sweetalert2.all.min.js"></script>
    <!-- Loading Overlay -->
    <script src="assets/loading/loadingoverlay.min.js"></script>
    
    <script>
      $(document).ready(function () {

        $("#cek-koneksi").click(function (e) { 
          e.preventDefault();
          var hostname = $("input[name='dbhostname']").val();
          var username = $("input[name='dbusername']").val();
          var pass = $("input[name='dbpass']").val();
          var name = $("input[name='dbname']").val();
          
          $.ajax({
            type: "POST",
            url: "<?php echo "koneksi.php"; ?>",
            data: {hostname: hostname,username: username,pass: pass, name: name},
            success: function (response) {
              console.log(response.error);
              if (response.error == false) {
                swal({
                  position: 'top-end',
                  type: 'success',
                  title: 'Koneksi berhasil',
                  showConfirmButton: false,
                  timer: 1500
                })
              } else {
                swal({
                  position: 'top-end',
                  type: 'error',
                  title: 'Koneksi gagal',
                  showConfirmButton: false,
                  timer: 1500
                })
              }         
            }
          });
        });

        $("#btn-tambah").click(function (e) { 
          e.preventDefault();
          var num = new_number();
          $($("#table-config").find($('.data-row').last()).clone()).insertBefore("#btn-tambah");
          $("#table-config").find($('.data-row').last()).find("label[for='column-name']").text("Kolom " + num + " :");
          $("#table-config").find($('.data-row').last()).find(".col-name").attr("name","col-" + num).val("");
          $("#table-config").find($('.data-row').last()).find(".data-type").attr("name","data-" + num).val("");
          
        });

        $("#btn-submit").click(function (e) { 
          e.preventDefault();
          var len = $(".data-row").length;
          $("input[name='len']").val(len);
          var data = $("#main-form").serialize();

          $.ajax({
            type: "POST",
            url: "<?php echo "generate.php"; ?>",
            data: data,
            beforeSend: function() {
              $.LoadingOverlay("show", {
                  image       : "",
                  fontawesome : "fas fa-cog fa-spin"
              });
            },
            success: function (response) {
              $.LoadingOverlay("hide");
              console.log(response);
              if (response.error == false) {
                swal({
                  type: 'success',
                  title: 'Success',
                  text: response.row + ' Row affected',
                })
              } else {
                swal({
                  type: 'error',
                  title: 'Oops...',
                  html: 'Something went wrong!</br>' + response.row + ' Row affected',
                })
                
              }
            }
          });
        });
      });

      function new_number() {  
        var name = $("#table-config").find($('.data-row').last()).find("label[for='column-name']").text();
        var new_num = parseInt(name.substring(6, 7)) + 1; 
        return new_num;
      }

    </script>
  </body>

</html>
